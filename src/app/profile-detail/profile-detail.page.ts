import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PlayerTimingModalPage } from '../models/player-timing-modal/player-timing-modal.page';
import { GlobalMothodsService } from 'src/helpers/global/global-mothods.service';
import { ActivatedRoute } from '@angular/router';
import { PlayerProfileTipPage } from '../models/player-profile-tip/player-profile-tip.page';
import { HttpClientService } from 'src/helpers/http/http-client.service';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-profile-detail',
  templateUrl: './profile-detail.page.html',
  styleUrls: ['./profile-detail.page.scss'],
})
export class ProfileDetailPage implements OnInit {
  userid:any;
  date:Date;
  sleepDetail:any;
  foodDetail:any;
  sleepHours=0;
  foodCount=0;
  workoutDetail:any;
  workoutTime=0;
  stretchTime=0;
  stretchDetail:any;
  month:any;
  constructor(public modalController: ModalController,public globalService: GlobalMothodsService,private route: ActivatedRoute,public httpService: HttpClientService,) {
    this.date=new Date();
   }

  ngOnInit() {
    this.route.params.subscribe(params => this.userid = params.id);
    this.sleepDetails();
    this.foodDetails();
    this.workoutDetails();
    this.stretchDetails();
    
    this.month=this.date.getMonth()+1;
  }
  
  async playerTimingsModal(){
    const modal = await this.modalController.create({
      component: PlayerTimingModalPage,
      componentProps: {
        foodDetail: this.foodDetail,
           },
      cssClass: 'PlayerTimingModal'
    });
    modal.present();
  }
  sleepDetails()
  {
    this.month=this.date.getMonth()+1; 
    this.globalService.displayLoader();
    this.httpService.post(environment.baseUrl, 'sleepDetails.php?id='+this.userid+'&date='+this.date.getFullYear()+'-'+this.month+'-'+this.date.getDate() ).then(result => {
      this.sleepDetail = result;
      if (this.sleepDetail.status == this.globalService.statusCode) {
        this.globalService.closeLoader();  
        this.sleepHours=this.sleepDetail.sleepHours;
      }
      else if (this.sleepDetail.status==404){
        this.globalService.closeLoader(); 
        this.sleepHours=0;
      }
    });
  }
  foodDetails()
  {
    this.month=this.date.getMonth()+1;
    this.globalService.displayLoader();
  
    this.httpService.post(environment.baseUrl, 'foodDetails.php?id='+this.userid+'&date='+this.date.getFullYear()+'-'+this.month+'-'+this.date.getDate() ).then(result => {
      this.foodDetail = result;
      
      if (this.foodDetail.status == this.globalService.statusCode) {
        this.globalService.closeLoader();  
        this.foodCount=this.foodDetail.data.length;
      }
      else if (this.foodDetail.status==404){
        this.globalService.closeLoader(); 
        this.foodCount=0;
      }
    });
  }
  workoutDetails()
  {
    this.workoutTime=0;
    this.month=this.date.getMonth()+1;
    this.globalService.displayLoader();
    
    this.httpService.post(environment.baseUrl, 'workoutDetails.php?id='+this.userid+'&date='+this.date.getFullYear()+'-'+this.month+'-'+this.date.getDate() ).then(result => {
      this.workoutDetail = result;
      
      if (this.workoutDetail.status == this.globalService.statusCode) {
        this.globalService.closeLoader();  
        this.workoutTime=this.workoutDetail.totalTime;
      }
      else if (this.workoutDetail.message=='No Record Found'){
        this.globalService.closeLoader(); 
        this.workoutTime=0;
      }
    });
  }
  stretchDetails()
  {
    this.stretchTime=0;
    this.month=this.date.getMonth()+1;
    this.globalService.displayLoader();
    debugger
    this.httpService.post(environment.baseUrl, 'stretchingDetails.php?id='+this.userid+'&date='+this.date.getFullYear()+'-'+this.month+'-'+this.date.getDate() ).then(result => {
      this.stretchDetail = result;
      debugger
      if (this.stretchDetail.status == this.globalService.statusCode) {
        this.globalService.closeLoader();  
        this.stretchTime=this.stretchDetail.totalTime;
      }
      else if (this.stretchDetail.message=='No Record Found'){
        this.globalService.closeLoader(); 
        this.stretchTime=0;
      }
    });
  }
  onChangeDate(ev)
  {
    console.log(ev);
    this.date= new Date(ev.detail.value);
    this.sleepDetails();
    this.foodDetails();
    this.workoutDetails();
    this.stretchDetails();

  }
}
