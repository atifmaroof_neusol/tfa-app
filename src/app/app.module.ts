import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { RoundProgressModule, ROUND_PROGRESS_DEFAULTS } from 'angular-svg-round-progressbar';
import { SecondsToTimePipe } from './pipes/seconds-to-time.pipe';
import { BaseService } from "../helpers/commom/base.service";
import { GlobalMothodsService } from "../helpers/global/global-mothods.service";
import { HttpClientService } from "../helpers/http/http-client.service";
import { HttpClientModule } from '@angular/common/http';
import { PlayerProfileTipPageModule } from './models/player-profile-tip/player-profile-tip.module';
import { PlayerTimingModalPageModule } from './models/player-timing-modal/player-timing-modal.module';
import { DatePipe } from '@angular/common';
@NgModule({
  declarations: [AppComponent, SecondsToTimePipe],
  imports: [BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    RoundProgressModule,
    HttpClientModule,
    PlayerProfileTipPageModule,
    PlayerTimingModalPageModule
  ],
  providers: [
    DatePipe,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    BaseService, GlobalMothodsService, HttpClientService,
    {
      provide: ROUND_PROGRESS_DEFAULTS,
      useValue: {
        color: '#f00',
        background: '#ffff'
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
