import { Component, OnInit } from '@angular/core';
import { ValidationsService } from '../../helpers/Validators/validations.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpClientService } from 'src/helpers/http/http-client.service';
import { GlobalMothodsService } from 'src/helpers/global/global-mothods.service';
import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { Events, NavController } from '@ionic/angular';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  result: any;
  userObj: any;
  constructor(public validations: ValidationsService,
    public formBuilder: FormBuilder,
   public httpService: HttpClientService, 
   public globalService: GlobalMothodsService,
   public router: Router,
   public events: Events,public navCtrl: NavController,) {
    this.loginForm = this.formBuilder.group({
     
      password: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
      ]))
    });
    }

  ngOnInit() {
    
    
  }
  login()
  {
    debugger
    if (this.loginForm.valid) {
      this.globalService.displayLoader();
      this.httpService.post(environment.baseUrl, 'login.php?email='+this.loginForm.value.email+'&password='+this.loginForm.value.password).then(result => {
        this.userObj = result;
        debugger
        if (this.userObj.status == this.globalService.statusCode) {
          debugger
          this.globalService.createLocalStorageItem('userObj', this.userObj.data);
          this.events.publish('user:loggedin', this.userObj.data);
          this.globalService.closeLoader();  
          if(this.userObj.data.roleCode=='PLAYER')
          {
          this.navCtrl.navigateRoot(
          ['/profile',this.userObj.data.id]
          );
        }
        else
        {
          this.navCtrl.navigateRoot('/player-list');
        }
        }
        else{
          this.globalService.presentToast('Username or Password is incorrect');
          this.globalService.closeLoader();   
        }
      });
    }
    else {
      this.validations.validateAllFormFields(this.loginForm);
    }
  }
}
