import { Component, OnInit } from '@angular/core';
import { ValidationsService } from '../../helpers/Validators/validations.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpClientService } from 'src/helpers/http/http-client.service';
import { GlobalMothodsService } from 'src/helpers/global/global-mothods.service';
import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { Events, NavController } from '@ionic/angular';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-workout-start',
  templateUrl: './workout-start.page.html',
  styleUrls: ['./workout-start.page.scss'],
})
export class WorkoutStartPage implements OnInit {
  workoutForm: FormGroup;
  type = 'WORKOUT';
  userObj: any;
  userId: any;
  result: any;
  workoutName:any;
  constructor(public validations: ValidationsService,
    public formBuilder: FormBuilder,
    public httpService: HttpClientService,
    public globalService: GlobalMothodsService,
    public router: Router,
    public events: Events, public navCtrl: NavController,private route: ActivatedRoute, public datepipe: DatePipe) {
      this.route.params.subscribe(params => this.workoutName = params.workoutName);
    this.userObj = this.globalService.getLocalStorageItem('userObj');
    this.userId = this.userObj.id;
    this.workoutForm = this.formBuilder.group({
      startTime: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      endTime: new FormControl('', Validators.compose([
        Validators.required,
      ]))
    });
  }

  ngOnInit() {
  }
  addWorkout()
  {
    if (this.workoutForm.valid) {
      this.workoutForm.value.startTime=  this.datepipe.transform(  this.workoutForm.value.startTime, 'yyyy-MM-dd H:mm:ss.SSSSSS');
      this.workoutForm.value.endTime=  this.datepipe.transform(  this.workoutForm.value.endTime, 'yyyy-MM-dd H:mm:ss.SSSSSS');
      this.globalService.displayLoader();
      this.httpService.post(environment.baseUrl, 'addFoodWorkout.php?userId=' + this.userId + '&type=' + this.type + '&foodWorkoutName='+this.workoutName + '&startTime=' + this.workoutForm.value.startTime+'&endTime='+this.workoutForm.value.endTime)
        .then((result) => {
          debugger
          if (result) {
            this.result = result;
            if (this.result.status == 200) {
              this.globalService.presentToast('Success');
              this.router.navigate(['/home'])
            }
            else {
              this.globalService.presentToast('Something went wrong. Please try again');
            }
            this.globalService.closeLoader();
          }
          else {
            this.globalService.closeLoader();
          }
        }, (err) => {
          this.globalService.closeLoader();
        });
    
  }
  else {
    this.validations.validateAllFormFields(this.workoutForm);
  }
  }
}
