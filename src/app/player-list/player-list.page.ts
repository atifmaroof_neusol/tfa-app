import { Component, OnInit } from '@angular/core';
import { GlobalMothodsService } from 'src/helpers/global/global-mothods.service';
import { environment } from 'src/environments/environment';
import { HttpClientService } from 'src/helpers/http/http-client.service';

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.page.html',
  styleUrls: ['./player-list.page.scss'],
})
export class PlayerListPage implements OnInit {
playerList:any;
  constructor(public globalService: GlobalMothodsService,public httpService: HttpClientService,) { }

  ngOnInit() {
   this.getPlayerList();
  }
  getPlayerList()
  {
    this.globalService.displayLoader();
    this.httpService.post(environment.baseUrl, 'getPlayerList.php').then(result => {
      this.playerList = result;
      debugger
      if (this.playerList.status == this.globalService.statusCode) {
        this.globalService.closeLoader();  
      }
      else{
        this.globalService.presentToast('Something Went Wrong');
        this.globalService.closeLoader();   
      }
    });
  }
}
