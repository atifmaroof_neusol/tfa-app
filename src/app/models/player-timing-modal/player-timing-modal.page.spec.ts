import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerTimingModalPage } from './player-timing-modal.page';

describe('PlayerTimingModalPage', () => {
  let component: PlayerTimingModalPage;
  let fixture: ComponentFixture<PlayerTimingModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerTimingModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerTimingModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
