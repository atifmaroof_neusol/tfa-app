import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';

@Component({
  selector: 'app-player-timing-modal',
  templateUrl: './player-timing-modal.page.html',
  styleUrls: ['./player-timing-modal.page.scss'],
})
export class PlayerTimingModalPage implements OnInit {
foodDetails:any;
  constructor(public navParams: NavParams) {
    this.foodDetails= this.navParams.get('foodDetail');
   }

  ngOnInit() {
  }

}
