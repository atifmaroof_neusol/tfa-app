import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PlayerProfileTipPage } from './player-profile-tip.page';

const routes: Routes = [
  {
    path: '',
    component: PlayerProfileTipPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PlayerProfileTipPage]
})
export class PlayerProfileTipPageModule {}
