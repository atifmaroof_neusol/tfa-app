import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { GlobalMothodsService } from 'src/helpers/global/global-mothods.service';
import { ActivatedRoute } from '@angular/router';
import { PlayerProfileTipPage } from '../models/player-profile-tip/player-profile-tip.page';
import { HttpClientService } from 'src/helpers/http/http-client.service';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  roleCode:any;
  userid:any;
  userProfile:any;
  date:Date;
  sleepDetail:any;
  foodDetail:any;
  sleepHours=0;
  foodCount=0;
  workoutDetail:any;
  workoutTime=0;
  stretchTime=0;
  stretchDetail:any;
  month:any;
  constructor(public modalController: ModalController,public globalService: GlobalMothodsService,private route: ActivatedRoute,public httpService: HttpClientService,) { }

  ngOnInit() {
    this.roleCode=this.globalService.getLocalStorageItem('userObj')
    this.roleCode=this.roleCode.roleCode;
    this.route.params.subscribe(params => this.userid = params.id);
    this.getprofile();
    this.sleepDetails();
    this.foodDetails();
    this.workoutDetails();
    this.stretchDetails();
    setTimeout(() => {
      var a=document.getElementsByTagName('body')[0].innerText;
console.log(a);
    }, 5000);
  }
  async openProfileTipModal() {
    debugger
    const modal = await this.modalController.create({
      component: PlayerProfileTipPage,
      cssClass: 'playerProfileTipModal'
    });
    modal.present();
  }
  getprofile()
  {
    this.globalService.displayLoader();
    this.httpService.post(environment.baseUrl, 'getPlayerprofile.php?id='+this.userid).then(result => {
      this.userProfile = result;
      if (this.userProfile.status == this.globalService.statusCode) {
        this.globalService.closeLoader();  
      }
      else{
        this.globalService.presentToast('Something Went Wrong');
        this.globalService.closeLoader();   
      }
    });
  }
  sleepDetails()
  {
    this.date=new Date();
    this.month=this.date.getMonth()+1;
    debugger
    this.globalService.displayLoader();
    this.httpService.post(environment.baseUrl, 'sleepDetails.php?id='+this.userid+'&date='+this.date.getFullYear()+'-'+this.month+'-'+this.date.getDate() ).then(result => {
      this.sleepDetail = result;
      if (this.sleepDetail.status == this.globalService.statusCode) {
        this.globalService.closeLoader();  
        this.sleepHours=this.sleepDetail.sleepHours;
      }
      else if (this.sleepDetail.status==404){
        this.sleepHours=0;
      }
    });
  }
  foodDetails()
  {
    this.date=new Date();
    this.month=this.date.getMonth()+1;
    this.globalService.displayLoader();
    debugger
    this.httpService.post(environment.baseUrl, 'foodDetails.php?id='+this.userid+'&date='+this.date.getFullYear()+'-'+this.month+'-'+this.date.getDate() ).then(result => {
      this.foodDetail = result;
      debugger
      if (this.foodDetail.status == this.globalService.statusCode) {
        this.globalService.closeLoader();  
        this.foodCount=this.foodDetail.data.length;
      }
      else if (this.foodDetail.status==404){
        this.foodCount=0;
      }
    });
  }
  workoutDetails()
  {
    this.date=new Date();
    this.month=this.date.getMonth()+1;
    this.globalService.displayLoader();
    debugger
    this.httpService.post(environment.baseUrl, 'workoutDetails.php?id='+this.userid+'&date='+this.date.getFullYear()+'-'+this.month+'-'+this.date.getDate() ).then(result => {
      this.workoutDetail = result;
      debugger
      if (this.workoutDetail.status == this.globalService.statusCode) {
        this.globalService.closeLoader();  
        this.workoutTime=this.workoutDetail.totalTime;
      }
      else if (this.workoutDetail.status==404){
        this.workoutTime=0;
      }
    });
  }
  stretchDetails()
  {
    this.date=new Date();
    this.month=this.date.getMonth()+1;
    this.globalService.displayLoader();
    debugger
    this.httpService.post(environment.baseUrl, 'stretchingDetails.php?id='+this.userid+'&date='+this.date.getFullYear()+'-'+this.month+'-'+this.date.getDate() ).then(result => {
      this.stretchDetail = result;
      debugger
      if (this.stretchDetail.status == this.globalService.statusCode) {
        this.globalService.closeLoader();  
        this.stretchTime=this.stretchDetail.totalTime;
      }
      else if (this.workoutDetail.status==404){
        this.stretchTime=0;
      }
    });
  }
}