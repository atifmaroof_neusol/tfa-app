import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomePageModule) },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
  { path: 'stretching', loadChildren: './stretching/stretching.module#StretchingPageModule' },
  { path: 'workout-list', loadChildren: './workout-list/workout-list.module#WorkoutListPageModule' },
  { path: 'workout-start/:workoutName', loadChildren: './workout-start/workout-start.module#WorkoutStartPageModule' },
  { path: 'sleep', loadChildren: './sleep/sleep.module#SleepPageModule' },
  { path: 'profile/:id', loadChildren: './profile/profile.module#ProfilePageModule' },
  { path: 'profile-detail/:id', loadChildren: './profile-detail/profile-detail.module#ProfileDetailPageModule' },
  { path: 'player-list', loadChildren: './player-list/player-list.module#PlayerListPageModule' },
  { path: 'player-profile-tip', loadChildren: './models/player-profile-tip/player-profile-tip.module#PlayerProfileTipPageModule' },
  { path: 'player-timing-modal', loadChildren: './models/player-timing-modal/player-timing-modal.module#PlayerTimingModalPageModule' },
  { path: 'add-food', loadChildren: './add-food/add-food.module#AddFoodPageModule' },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
