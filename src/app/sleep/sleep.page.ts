import { Component, OnInit } from '@angular/core';
import { ValidationsService } from '../../helpers/Validators/validations.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpClientService } from 'src/helpers/http/http-client.service';
import { GlobalMothodsService } from 'src/helpers/global/global-mothods.service';
import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { Events, NavController } from '@ionic/angular';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-sleep',
  templateUrl: './sleep.page.html',
  styleUrls: ['./sleep.page.scss'],
})
export class SleepPage implements OnInit {
  sleepForm: FormGroup;
  type = 'SLEEP';
  userObj: any;
  userId: any;
  result: any;
  constructor(public validations: ValidationsService,
    public formBuilder: FormBuilder,
    public httpService: HttpClientService,
    public globalService: GlobalMothodsService,
    public router: Router,
    public events: Events, public navCtrl: NavController,
    public datepipe: DatePipe) {
    this.userObj = this.globalService.getLocalStorageItem('userObj');
    this.userId = this.userObj.id;
    this.sleepForm = this.formBuilder.group({
      startTime: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      endTime: new FormControl('', Validators.compose([
        Validators.required,
      ]))
    });
  }

  ngOnInit() {
  }
  addSleep(){
    if (this.sleepForm.valid) {
      this.sleepForm.value.startTime=  this.datepipe.transform(  this.sleepForm.value.startTime, 'yyyy-MM-dd H:mm:ss.SSSSSS');
      this.sleepForm.value.endTime=  this.datepipe.transform(  this.sleepForm.value.endTime, 'yyyy-MM-dd H:mm:ss.SSSSSS');
      this.globalService.displayLoader();
      this.httpService.post(environment.baseUrl, 'addFoodWorkout.php?userId=' + this.userId + '&type=' + this.type + '&foodWorkoutName=Sleep' + '&startTime=' + this.sleepForm.value.startTime+'&endTime='+this.sleepForm.value.endTime)
        .then((result) => {
          debugger
          if (result) {
            this.result = result;
            if (this.result.status == 200) {
              this.globalService.presentToast('Success');
              this.globalService.closeLoader();
              this.router.navigate(['/home'])
            }
            else {
              this.globalService.presentToast('Something went wrong. Please try again');
            }
          }
          else {
            this.globalService.closeLoader();
          }
        }, (err) => {
          this.globalService.closeLoader();
        });
    
  }
  else {
    this.validations.validateAllFormFields(this.sleepForm);
  }
  }

}
