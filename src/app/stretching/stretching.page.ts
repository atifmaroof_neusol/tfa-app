import { Component, OnInit } from '@angular/core';

import { from } from 'rxjs';
import { ValidationsService } from '../../helpers/Validators/validations.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpClientService } from 'src/helpers/http/http-client.service';
import { GlobalMothodsService } from 'src/helpers/global/global-mothods.service';
import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { Events, NavController } from '@ionic/angular';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-stretching',
  templateUrl: './stretching.page.html',
  styleUrls: ['./stretching.page.scss'],

})
export class StretchingPage implements OnInit {
  addStretchingForm: FormGroup;
  type = 'STRETCHING';
  userObj: any;
  userId: any;
  result: any;
  constructor(public validations: ValidationsService,
    public formBuilder: FormBuilder,
    public httpService: HttpClientService,
    public globalService: GlobalMothodsService,
    public router: Router,
    public events: Events, public navCtrl: NavController, public datepipe: DatePipe) {
    this.userObj = this.globalService.getLocalStorageItem('userObj');
    this.userId = this.userObj.id;
    this.addStretchingForm = this.formBuilder.group({
      startTime: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      endTime: new FormControl('', Validators.compose([
        Validators.required,
      ]))
    });
  }

  ngOnInit() {

  }
  addStretching()
  {
    if (this.addStretchingForm.valid) {
      this.addStretchingForm.value.startTime=  this.datepipe.transform(  this.addStretchingForm.value.startTime, 'yyyy-MM-dd H:mm:ss.SSSSSS');
      this.addStretchingForm.value.endTime=  this.datepipe.transform(  this.addStretchingForm.value.endTime, 'yyyy-MM-dd H:mm:ss.SSSSSS');
      this.globalService.displayLoader();
      this.httpService.post(environment.baseUrl, 'addFoodWorkout.php?userId=' + this.userId + '&type=' + this.type + '&foodWorkoutName=Stretching' + '&startTime=' + this.addStretchingForm.value.startTime+'&endTime='+this.addStretchingForm.value.endTime)
        .then((result) => {
          debugger
          if (result) {
            this.result = result;
            if (this.result.status == 200) {
              this.globalService.presentToast('Success');
              this.globalService.closeLoader();
              this.router.navigate(['/home']);
            }
            else {
              this.globalService.presentToast('Something went wrong. Please try again');
            }
          }
          else {
            this.globalService.closeLoader();
          }
        }, (err) => {
          this.globalService.closeLoader();
        });
    
  }
  else {
    this.validations.validateAllFormFields(this.addStretchingForm);
  }
  }
}
