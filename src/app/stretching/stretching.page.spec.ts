import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StretchingPage } from './stretching.page';

describe('StretchingPage', () => {
  let component: StretchingPage;
  let fixture: ComponentFixture<StretchingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StretchingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StretchingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
