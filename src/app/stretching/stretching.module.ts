import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RoundProgressModule, ROUND_PROGRESS_DEFAULTS } from 'angular-svg-round-progressbar';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { StretchingPage } from './stretching.page';
import { from } from 'rxjs';
const routes: Routes = [
  {
    path: '',
    component: StretchingPage
  }
];

@NgModule({
  imports: [
    RoundProgressModule,
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
  
    RouterModule.forChild(routes)
  ],
  providers: [{
    provide: ROUND_PROGRESS_DEFAULTS,
    useValue: {
      color: '#f00',
      background: '#ffff'
    }
  }],
  declarations: [StretchingPage]
})
export class StretchingPageModule { }
