import { Component, OnInit } from '@angular/core';
import { ValidationsService } from '../../helpers/Validators/validations.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpClientService } from 'src/helpers/http/http-client.service';
import { GlobalMothodsService } from 'src/helpers/global/global-mothods.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
let baseURL = environment.baseUrl;

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  coachForm: FormGroup;
  playerForm: FormGroup;
  result: any;
  public activeSegment: any;
  constructor(public validations: ValidationsService,
     public formBuilder: FormBuilder,
    public httpService: HttpClientService, 
    public globalService: GlobalMothodsService,
    public router: Router) {
    this.activeSegment = "coach";
    this.coachForm = this.formBuilder.group({
      fullName: new FormControl('', Validators.compose([
        Validators.required
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      confirmPassword: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      roleCode: new FormControl('COACH'),
      email: new FormControl('', Validators.compose([
        Validators.required,
      ]))
    });
    this.playerForm = this.formBuilder.group({
      fullName: new FormControl('', Validators.compose([
        Validators.required
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      confirmPassword: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      position: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      height: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      weight: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      roleCode: new FormControl('PLAYER'),
      email: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      gender: new FormControl('', Validators.compose([
        Validators.required,
      ]))
    });
  }

  ngOnInit() {
  }
  segmentChanged(ev: any) {
    debugger;
    this.activeSegment = ev.detail.value;
    //console.log('Segment changed', ev);
  }
  signup() {
    debugger
    if (this.activeSegment == 'coach') {
      if (this.coachForm.valid) {
        if (this.coachForm.value.password != this.coachForm.value.confirmPassword) { this.globalService.presentToast('password does not match'); }
        else {
          this.globalService.displayLoader();
          this.httpService.post(environment.baseUrl, 'addUser.php?fullName=' + this.coachForm.value.fullName + '&email=' + this.coachForm.value.email + '&roleCode=' + this.coachForm.value.roleCode + '&password=' + this.coachForm.value.password)
            .then((result) => {
              debugger
              if (result) {
                this.result = result;
                if (this.result.status == 200) {
                  this.globalService.presentToast('Signup successfull');
                  this.router.navigate(['/login'])
                }
                else {
                  this.globalService.presentToast('Something went wrong. Please try again');
                }
                this.globalService.closeLoader();
              }
              else {
                this.globalService.closeLoader();
              }
            }, (err) => {
              this.globalService.closeLoader();
            });
        }
      }
      else {
        this.validations.validateAllFormFields(this.coachForm);
      }
    }
    else
    {
      if (this.playerForm.valid) {
        if (this.playerForm.value.password != this.playerForm.value.confirmPassword) { this.globalService.presentToast('password does not match'); }
        else {
          this.globalService.displayLoader();
          this.httpService.post(environment.baseUrl, 'addUser.php?fullName=' + this.playerForm.value.fullName + '&email=' + this.playerForm.value.email + '&roleCode=' + this.playerForm.value.roleCode + '&password=' + this.playerForm.value.password+ '&height=' + this.playerForm.value.height+ '&weight=' + this.playerForm.value.weight+ '&position=' + this.playerForm.value.position+'&gender='+this.playerForm.value.gender)
            .then((result) => {
              debugger
              if (result) {
                this.result = result;
                if (this.result.status == 200) {
                  this.globalService.presentToast('Signup successfull');
                  this.router.navigate(['/login'])
                }
                else {
                  this.globalService.presentToast('Something went wrong. Please try again');
                }
                this.globalService.closeLoader();
              }
              else {
                this.globalService.closeLoader();
              }
            }, (err) => {
              this.globalService.closeLoader();
            });
        }
      }
      else {
        this.validations.validateAllFormFields(this.playerForm);
      }
    }
  }
 
}
