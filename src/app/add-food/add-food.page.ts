import { Component, OnInit } from '@angular/core';
import { ValidationsService } from '../../helpers/Validators/validations.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpClientService } from 'src/helpers/http/http-client.service';
import { GlobalMothodsService } from 'src/helpers/global/global-mothods.service';
import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { Events, NavController } from '@ionic/angular';
import { DatePipe } from '@angular/common'
import { DeprecatedDatePipe } from '@angular/common';
@Component({
  selector: 'app-add-food',
  templateUrl: './add-food.page.html',
  styleUrls: ['./add-food.page.scss'],
})
export class AddFoodPage implements OnInit {
  addFoodForm: FormGroup;
  type = 'FOOD';
  userObj: any;
  userId: any;
  result: any;
  constructor(public validations: ValidationsService,
    public formBuilder: FormBuilder,
    public httpService: HttpClientService,
    public globalService: GlobalMothodsService,
    public router: Router,
    public events: Events,
     public navCtrl: NavController,
    public datepipe: DatePipe) {
    this.userObj = this.globalService.getLocalStorageItem('userObj');
    this.userId = this.userObj.id;
    this.addFoodForm = this.formBuilder.group({

      startTime: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      foodWorkoutName: new FormControl('', Validators.compose([
        Validators.required,
      ]))
      ,
      foodDescription: new FormControl('', Validators.compose([
        Validators.required,
      ]))
    });
  }

  ngOnInit() {
  }
  addFood(){
    debugger
    if (this.addFoodForm.valid) {
      this.addFoodForm.value.startTime=  this.datepipe.transform(  this.addFoodForm.value.startTime, 'yyyy-MM-dd H:mm:ss.SSSSSS');
        this.globalService.displayLoader();
        this.httpService.post(environment.baseUrl, 'addFoodWorkout.php?userId=' + this.userId + '&type=' + this.type + '&foodWorkoutName=' + this.addFoodForm.value.foodWorkoutName + '&startTime=' + this.addFoodForm.value.startTime+'&foodDescription='+this.addFoodForm.value.foodDescription)
          .then((result) => {
            debugger
            if (result) {
              this.result = result;
              if (this.result.status == 200) {
                this.globalService.closeLoader();
                this.globalService.presentToast('Success');
                
                this.router.navigate(['/home'])
              }
              else {
                this.globalService.presentToast('Something went wrong. Please try again');
              }
              
            }
            else {
              this.globalService.closeLoader();
            }
          }, (err) => {
            this.globalService.closeLoader();
          });
      
    }
    else {
      this.validations.validateAllFormFields(this.addFoodForm);
    }
  }
}
