import { Injectable } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidationsService {

  constructor() { 
    
  }
  validation_messages = {

    'email': [
      { type: 'required', message: 'Please enter your email address', messageAr: "يرجى ادخال بريدك الالكتروني " },
      { type: 'pattern', message: 'Please enter a valid email address (example: abcd@xyz.com)', messageAr: "(مثال: abcd@xyz.com) يرجى ادخال بريد الكتروني فعال " }
    ],
    'UserName': [
      { type: 'required', message: 'Please enter your username', messageAr: 'يرجى ادخال اسم مستخدم' },
      { type: 'minlength', message: 'Please enter a username with at least 5 characters and at most 15 characters', messageAr: 'يرجى ادخال اسم مستخدم يتكون من 5 أحرف بحد أدنى و 15 أحرف بحد أقصى' },
      { type: 'maxlength', message: 'Please enter a username with at least 5 characters and at most 15 characters', messageAr: 'يرجى ادخال اسم مستخدم يتكون من 5 أحرف بحد أدنى و 15 أحرف بحد أقصى' },
      { type: 'pattern', message: 'Please enter a valid username', messageAr: 'يرجى ادخال اسم مستخدم صالح' }
    ],
    'Password': [
      { type: 'required', message: 'Please enter your password', messageAr: 'يرجى تحديد كلمة مرور  خاصة بك' },
      { type: 'minlength', message: 'Please enter a password with at least 6 characters and at most 30 characters', messageAr: 'يرجى ادخال كلمة المرور بحد أدنى 6 حروف وبحد أقصى 30 حرفا' },
      { type: 'maxlength', message: 'Please enter a password with at least 6 characters and at most 30 characters', messageAr: 'يرجى ادخال كلمة المرور بحد أدنى 6 حروف وبحد أقصى 30 حرفا' },
      { type: 'pattern', message: 'Your password must contain 6 characters and at least 1 number ', messageAr: 'كلمة المرور يجب أن تحتوي على 6 أحرف وعلى الأقل رقم واحد' }
    ],
    'phone': [
      { type: 'required', message: 'This field is required.', messageAr: 'هذا الحقل مطلوب. يرجى إدخال قيمة.' },
    ],
    'subject': [
      { type: 'required', message: 'This field is required.', messageAr: 'هذا الحقل مطلوب. يرجى إدخال قيمة.' },
    ],
    'notes': [
      { type: 'required', message: 'This field is required.', messageAr: 'هذا الحقل مطلوب. يرجى إدخال قيمة.' },
    ]

  };

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
