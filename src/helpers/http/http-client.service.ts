import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//let baseURL = environment.baseUrlV2;
@Injectable({
  providedIn: 'root'
})
export class HttpClientService {
  constructor(private http: HttpClient, ) {
  }
  get( host, apiURL) {
    return new Promise((resolve, reject) => {
      let req_headers = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin':'*'
      };
      let headers = new HttpHeaders(req_headers);
      this.http.get(host + apiURL, { headers: headers }).subscribe(res => {
        resolve(res);
      }, (err) => {
        debugger
      });
    });
  }

  post( host, apiURL) {
    return new Promise((resolve, reject) => {
      let req_headers = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin':'*'
      };
      let headers = new HttpHeaders(req_headers);
      this.http.post(host + apiURL, { header: headers }).subscribe(res => {
        resolve(res);
      }, (err) => {
        debugger
        resolve(err);
      });
    });
  }
}