import { Injectable, Renderer2, RendererFactory2, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { BehaviorSubject } from 'rxjs';
import { GlobalMothodsService } from '../global/global-mothods.service';

@Injectable({
  providedIn: 'root'
})

export class ThemeService {
  renderer: Renderer2;
  constructor(public globalService: GlobalMothodsService, private rendererFactory: RendererFactory2, @Inject(DOCUMENT) private document: Document) {
    this.renderer = this.rendererFactory.createRenderer(null, null);
    let htmlRoot: HTMLElement = <HTMLElement>document.getElementsByTagName("html")[0];
    if(!this.globalService.getLocalStorageItem('theme')){
      if (htmlRoot != null) {
        htmlRoot.id = 'medium';
      }
      let theme = new Themes();
      theme.fontSize = htmlRoot.id;
      theme.color='default';
      this.renderer.addClass(this.document.body, 'default');
      this.globalService.createLocalStorageItem('theme', theme);
    }
    else{
      htmlRoot.id =this.globalService.getLocalStorageItem('theme').fontSize;
      this.renderer.addClass(this.document.body, this.globalService.getLocalStorageItem('theme').color);
    }
  }

  enableDarkTheme() {
    this.removeClass();
    this.renderer.addClass(this.document.body, 'dark-theme');
  }
  enableGrey() {
    this.removeClass();
    this.renderer.addClass(this.document.body, 'grey-theme');
  }
  enableGreen() {
    this.removeClass();
    this.renderer.addClass(this.document.body, 'green-theme');
  }
  enableRed() {
    this.removeClass();
    this.renderer.addClass(this.document.body, 'red-theme');
  }
  enableDefault() {
    this.removeClass();
    this.renderer.addClass(this.document.body, 'default');
  }

  removeClass(){
    this.renderer.removeClass(this.document.body, 'dark-theme');
    this.renderer.removeClass(this.document.body, 'grey-theme');
    this.renderer.removeClass(this.document.body, 'green-theme');
    this.renderer.removeClass(this.document.body, 'red-theme');
    this.renderer.removeClass(this.document.body, 'default');
  }
}

export class Themes {
  fontSize: string;
  color: string;
}
