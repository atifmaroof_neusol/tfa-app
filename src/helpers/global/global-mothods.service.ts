import { Injectable } from '@angular/core';
import { MenuController, Events, LoadingController, AlertController, ToastController, NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class GlobalMothodsService {

  statusCode = "200";
  isLoading: any;

  constructor(public menuController: MenuController, public events: Events, public loadingController: LoadingController,
    public alertController: AlertController,
    public toastController: ToastController,public navctrl:NavController ) { }

  createLocalStorageItem(name, value) {
    if (name && value) {
      localStorage.setItem(name, JSON.stringify(value));
    }
  }

  public getLocalStorageItem(name) {
    if (name) {
      var result = JSON.parse(localStorage.getItem(name));
      return result;
    }
  }
  public getLocalStorageLanguage(name) {
    return localStorage.getItem(name);
  }

  removeLocalStorageItem(name) {
    if (name) {
      localStorage.removeItem(name);
    }
  }

  logout() {
    localStorage.removeItem('userObj');
    this.menuController.close();
    this.events.publish('user:loggedin', null);
    this.navctrl.navigateRoot('');
  }
  clearCommonStorage() {
    this.removeLocalStorageItem('userObj')
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  async openDialog(title, message) {
    const alert = await this.alertController.create({
      header: title,
      message: message,
      buttons: [
        {
          text: ('Ok'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }
      ]
    });
    await alert.present();
  }

  async displayLoader() {
    this.isLoading = true;
    return await this.loadingController.create({
      duration: 5000,
      spinner: 'bubbles',
      message: 'Please wait...' ,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    }).then(a => {
      a.present().then(() => {
        // console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() =>
            console.log('')
          );
        }
      });
    });
  }

  async closeLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log(''));
  }

  setMenuPages(userOb: any) {
    let user = {};
    //Guest
    if (!userOb) {
      user['image'] = "assets/img/userImage.jpg";
      user['name'] = "Guest";
    }
    else {
      //Freelancer
      user['name'] = userOb.UserName;
      user['image'] = "assets/img/userImage.jpg";
    }
    return user;
  }

}
