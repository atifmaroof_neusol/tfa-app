import { TestBed } from '@angular/core/testing';

import { GlobalMothodsService } from './global-mothods.service';

describe('GlobalMothodsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GlobalMothodsService = TestBed.get(GlobalMothodsService);
    expect(service).toBeTruthy();
  });
});
