import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
//import { ThemeableBrowser, ThemeableBrowserOptions, ThemeableBrowserObject } from '@ionic-native/themeable-browser/ngx';
@Injectable({
  providedIn: 'root'
})
export class BaseService {
  public lang: BehaviorSubject<String>;
  constructor() {
  }
}
